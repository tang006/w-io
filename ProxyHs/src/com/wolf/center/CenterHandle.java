package com.wolf.center;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.wolf.javabean.SystemNet;
import com.wolf.javabean.SystemNet.Cpaths;
import com.wolf.javabean.SystemNet.RegBean;
import com.wolf.locks.DistributedLockMg;
import com.wolf.locks.SystemLocks;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AttributeKey;

public class CenterHandle extends ChannelInboundHandlerAdapter{

	public static ConcurrentMap<String, Channel> channelMap = new ConcurrentHashMap<String,Channel>();//基本用于服务器之间调用
	private SystemNet.Datas.Builder idledatasBuilder = SystemNet.Datas.newBuilder();
	private String serverName = "";
	private Channel channel = null;
	private List<String> pathArray = null;//路径数组，所有注册的路径
	int pathArrayCount = 0;//路径数量
	public AtomicInteger serverRequestNum = null;
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		try {
			if(msg instanceof SystemNet.Datas){
				SystemNet.Datas.Builder datasBuilder = SystemNet.Datas.newBuilder();
				SystemNet.Datas datas = (SystemNet.Datas)msg;
				if(datas.getHandletype().equals("RegBean")){
					RegBean rb = datas.getRegBean();
					datasBuilder.setHandletype("RegBean");
					if(rb.getIp().trim().equals("") || rb.getPort().trim().equals("")){//没有ip或者port不允许注册
						datasBuilder.getSystemNetBeanBuilder().setMessage("ip or port is empty");
						ctx.writeAndFlush(datasBuilder);//发送注册后的信息
						datasBuilder.getSystemNetBeanBuilder().clear();
						SystemLocks.regLock.writeLock().unlock();
						return;
					}
					if(rb.getRegtype().equals("1")){//注册
						serverName = rb.getServerName();
						int serreqnum = rb.getRequestnum();
						if(serreqnum <= 0){
							serreqnum = 2;
						}
						
						Map<String,Cpaths> paths = rb.getPathsMap();
						
						serverRequestNum = new AtomicInteger(serreqnum*20);//请求数量，业务服务器配置*阻塞因子，(可作配置)
						ConcurrentMap<String,List<CenterHandle>> serverMap = CenterServer.serverMap;
						for(String key : paths.keySet()){
							String path = key;
							if(!serverMap.containsKey(path)){
								List<CenterHandle> list = new ArrayList<CenterHandle>();
								serverMap.put(path, list);
							}
							List<CenterHandle> list = serverMap.get(path);
							list.add(this);
							Cpaths cpaths = paths.get(key);
							List<String> locknames = paths.get(key).getLocknamesList();
							DistributedLockMg.INSTANCE.addLock(locknames, path, cpaths.getSecondnum(), cpaths.getGeWait());
						}
						channel = ctx.channel();
						datasBuilder.getSystemNetBeanBuilder().setMessage("Reg success");
						ctx.writeAndFlush(datasBuilder);
						datasBuilder.getSystemNetBeanBuilder().clear();
					}else if(rb.getRegtype().equals("0")){//脱离
						for(int i=0;i<pathArrayCount;i++){
							String path = pathArray.get(i)+"";
							ConcurrentMap<String,List<CenterHandle>> serverMap = CenterServer.serverMap;
							if(!serverMap.containsKey(path)){
								List<CenterHandle> list = new ArrayList<CenterHandle>();
								serverMap.put(path, list);
							}
							List<CenterHandle> list = serverMap.get(path);
							list.add(this);
							Iterator<CenterHandle> it = list.iterator();
							while(it.hasNext()){
								CenterHandle ch = it.next();
								if(ch.channel == this.channel){
									it.remove();
									break;
								}
							}
						}
						datasBuilder.getSystemNetBeanBuilder().setMessage("unReg success");
						ctx.writeAndFlush(datasBuilder);
						datasBuilder.getSystemNetBeanBuilder().clear();
					}
				}else{
					ctx.fireChannelRead(datas);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		super.exceptionCaught(ctx, cause);
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		for(int i=0;i<pathArrayCount;i++){
			ConcurrentMap<String,List<CenterHandle>> serverMap = CenterServer.serverMap;
			List<CenterHandle> list = serverMap.get(pathArray.get(i)+"");
			Iterator<CenterHandle> it = list.iterator();
			while(it.hasNext()){
				CenterHandle chle = it.next();
				if(chle.getChannel() == ctx.channel()){
					it.remove();
					System.out.println("remove:Node success");
					break;
				}
			}
			//serverMap.remove(pathArray.get(i)+"");
		}
		AttributeKey<String> channelIdAttr = AttributeKey.valueOf("channelId");
		String id = ctx.channel().attr(channelIdAttr).get();
		channelMap.remove(id);
		super.channelInactive(ctx);
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
			throws Exception {
		// TODO Auto-generated method stub
		if(evt instanceof IdleStateEvent){
			IdleStateEvent event = (IdleStateEvent)evt;
			if(event.state() == IdleState.READER_IDLE){
				idledatasBuilder.getSystemNetBeanBuilder().setMessage("is Running?");
				ctx.writeAndFlush(idledatasBuilder);
				idledatasBuilder.getSystemNetBeanBuilder().clear();
			}
		}
		super.userEventTriggered(ctx, evt);
	}

	public SystemNet.Datas.Builder getIdledatasBuilder() {
		return idledatasBuilder;
	}

	public void setIdledatasBuilder(SystemNet.Datas.Builder idledatasBuilder) {
		this.idledatasBuilder = idledatasBuilder;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public List<String> getPathArray() {
		return pathArray;
	}

	public void setPathArray(List<String> pathArray) {
		this.pathArray = pathArray;
	}

	public int getPathArrayCount() {
		return pathArrayCount;
	}

	public void setPathArrayCount(int pathArrayCount) {
		this.pathArrayCount = pathArrayCount;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		Channel channel = ctx.channel();
		channelMap.put(uuid, channel);
		AttributeKey<String> channelIdAttr = AttributeKey.valueOf("channelId");
		channel.attr(channelIdAttr).set(uuid);
		super.channelActive(ctx);
	}
	
}
