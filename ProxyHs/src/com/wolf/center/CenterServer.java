package com.wolf.center;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.wolf.javabean.SystemNet;
import com.wolf.javabean.SystemNet.RegBean;
import com.wolf.locks.DistributedLock;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

public class CenterServer implements Runnable{

	public static ConcurrentMap<String,List<CenterHandle>> serverMap = new ConcurrentHashMap<String,List<CenterHandle>>();//key:请求路径
	public static ConcurrentMap<String,String[]> serverLockMap = new ConcurrentHashMap<String,String[]>();//key:请求路径,value:锁名称
	public static ConcurrentMap<String,DistributedLock> locObjkMap = new ConcurrentHashMap<String,DistributedLock>();//key:锁名称,value:锁对象
	public static int regBeanLen = 16;
	//private int threadPoolNum = 30;
    //public static ExecutorService pool = null;//业务池
	
	//请求转发通道
	public void init(int port){
		//pool = Executors.newFixedThreadPool(threadPoolNum);
		ServerBootstrap boot = new ServerBootstrap();
		EventLoopGroup boss = new NioEventLoopGroup();
		EventLoopGroup worker = new NioEventLoopGroup(6);
		try{
			boot.group(boss, worker);
			boot.channel(NioServerSocketChannel.class);
			boot.childHandler(new ChannelInitializer<Channel>() {

				@Override
				protected void initChannel(Channel ch) throws Exception {//采用protobuf3作为传输的技术
					ch.pipeline().addLast(new ProtobufVarint32FrameDecoder());
					ch.pipeline().addLast(new ProtobufDecoder(SystemNet.Datas.getDefaultInstance()));
					ch.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
					ch.pipeline().addLast(new ProtobufEncoder());
					ch.pipeline().addLast(new CenterHandle());//注册中心handle
					ch.pipeline().addLast(new HttpServletResponseHandle());//需要返回前端数据相关处理的handle
					ch.pipeline().addLast(new ServerSendHandle());//服务之间相互调用
				}
			}).option(ChannelOption.SO_BACKLOG, 256);
			System.out.println("启动服务:"+port);
			boot.bind(port).sync().channel().closeFuture().sync();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			boss.shutdownGracefully();
			worker.shutdownGracefully();
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			init(9000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
