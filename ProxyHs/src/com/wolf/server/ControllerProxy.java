package com.wolf.server;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.google.gson.Gson;
import com.wolf.center.CenterHandle;
import com.wolf.center.CenterServer;
import com.wolf.center.ServerSendHandle;
import com.wolf.javabean.SystemNet;
import com.wolf.javabean.SystemNet.HttpServletRequest;
import com.wolf.javabean.SystemNet.HttpServletResponse;
import com.wolf.javabean.SystemNet.Locks;
import com.wolf.locks.DistributedLock;
import com.wolf.locks.DistributedLockMg;

import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.util.AttributeKey;

//将请求转发到业务服务器的控制器
public enum ControllerProxy {

	INSTANCE;
	
	public static String viewPath = "";
	public static String[] viewFileType;
	//key:responseId,value:HttpServerHandler
	public ConcurrentMap<String, HttpServerHandler> httpHandleMap = new ConcurrentHashMap<String, HttpServerHandler>();
	
	//请求转发
	public void sendConMethod(SystemNet.Datas.Builder datas,String path,String sessionId,HttpServerHandler httpHandle){
		if(sessionId != null && !sessionId.equals("null") && !sessionId.trim().equals("")){//sessionId不能为空
			boolean hasLock = false;//是否有锁
			DistributedLock dl = null;//锁对象
			String lockid = "";//锁Id
			if(CenterServer.serverLockMap.containsKey(path)){//判断路径是否包含锁
				dl = DistributedLockMg.INSTANCE.hasAndGet(path);
				if(dl == null){
					//没获得锁不允许进入方法，此处可加入其他处理逻辑;
					sendData(null, "get lock error", httpHandle.getCtxs().channel());
					return;
				}
				hasLock = true;
				lockid = UUID.randomUUID().toString().replace("-", "");
			}
			MySession session = SessionMsg.INSTANCE.getSession(sessionId);//获取session
			if(session!=null&&!session.isUnUse()){
				//将各种数据写入协议，发送到各个服务器
				datas.getHttpBeanBuilder().getRequestBuilder().getSessionBuilder().setSessionId(sessionId);
				datas.getHttpBeanBuilder().getRequestBuilder().getSessionBuilder().putAllSession(session.getAttr());
				session.setIsusing(true);
				
				ConcurrentMap<String,List<CenterHandle>> serverMap = CenterServer.serverMap;
				
				if(serverMap.get(path) == null){
					Channel httpChannel = httpHandle.getCtxs().channel();
					sendData(null, "path not found", httpChannel);
					return;
				}
				//nodes change
				//Channel channel = serverMap.get(path).get(0).getChannel();
				Channel channel = getServerChannel(serverMap.get(path),httpHandle);
				if(channel == null){
					sendData(null, "503 服务过载", httpHandle.getCtxs().channel());
					return;
				}
				
				//httpHandleList.add(httpHandle);
				String responseId = UUID.randomUUID().toString().replace("-", "");
				//httpHandle.setResponseId(responseId);
				httpHandleMap.put(responseId, httpHandle);
				datas.setHandletype("httpSend");
				datas.getHttpBeanBuilder().getResponseBuilder().setResponseId(responseId);
				datas.getHttpBeanBuilder().setUrl(path);
				Locks.Builder locksBuilder = Locks.newBuilder();
				locksBuilder.setId(lockid);
				datas.getHttpBeanBuilder().getRequestBuilder().setLocks(locksBuilder);
				System.out.println("发送锁id---"+lockid);
				channel.writeAndFlush(datas);
				
				session.setIsusing(false);
				if(hasLock){//如果已经获取了锁，则进行解锁监听
					DistributedLock.distributedLockMap.put(lockid, dl);
					dl.releaseLock();//等待解锁
				}
			}else{
				System.out.println("session is null");
				sendData(null, "session is null",httpHandle.getCtxs().channel());
				return;
			}
		}
	}
	
	//负载均衡器(下个版本将优化)，如果超过定义负载，将发送到下一个集群节点
	private Channel getServerChannel(List<CenterHandle> list,HttpServerHandler httpHandle){
		Iterator<CenterHandle> it = list.iterator();
		Channel channel = null;
		while(it.hasNext()){
			CenterHandle ch = it.next();
			int cur = ch.serverRequestNum.getAndDecrement();
			if(cur <= 0){
				ch.serverRequestNum.incrementAndGet();
				continue;
			}else{
				channel = ch.getChannel();
				httpHandle.centerHandle = ch;
				break;
			}
		}
		return channel;
	}
	
	//负载均衡器(下个版本将优化)，如果超过定义负载，将发送到下一个集群节点
	private Channel getServerChannel(List<CenterHandle> list){
		Iterator<CenterHandle> it = list.iterator();
		Channel channel = null;
		while(it.hasNext()){
			CenterHandle ch = it.next();
			int cur = ch.serverRequestNum.getAndDecrement();
			if(cur <= 0){
				ch.serverRequestNum.incrementAndGet();
				continue;
			}else{
				channel = ch.getChannel();
				if(channel != null){
					AttributeKey<String> channelIdAttr = AttributeKey.valueOf("channelId");
					String id = channel.attr(channelIdAttr).get();
					ServerSendHandle.centerHandle.put(id, ch);
				}
			}//这里和http的方式不太一样
		}
		return channel;
	}
	
	private static Gson gson = new Gson();
	
	//获取http通道
	private Channel getResponseChannel(String responseId){
		HttpServerHandler hsh = httpHandleMap.get(responseId);
		//负载还原
		hsh.centerHandle.serverRequestNum.incrementAndGet();
		Channel channel = hsh.getCtxs().channel();
		httpHandleMap.remove(responseId);
		return channel;
	}
	
	//发送数据或者文件到前端
	public void sendData(HttpServletResponse responses){
		
		Channel channel = getResponseChannel(responses.getResponseId());
	
	 	Map<String,String> map = responses.getAttrMap();
    	FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1, OK, Unpooled.wrappedBuffer(gson.toJson(map).getBytes(Charset.forName("utf-8"))));
        response.headers().set(CONTENT_TYPE, "text/plain");
        response.headers().set(CONTENT_LENGTH,
                response.content().readableBytes());
        response.headers().set("Access-Control-Allow-Origin","*");
        response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        channel.writeAndFlush(response);
        channel.flush();
        //ctx.close();
    }

	public void sendData(HttpServletResponse responses,String message,Channel channel){
		
		if(channel == null){
			channel = getResponseChannel(responses.getResponseId());
		}
		
    	FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1, OK, Unpooled.wrappedBuffer(message.getBytes(Charset.forName("utf-8"))));
        response.headers().set(CONTENT_TYPE, "text/plain");
        response.headers().set(CONTENT_LENGTH,
                response.content().readableBytes());
        response.headers().set("Access-Control-Allow-Origin","*");
        response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        channel.writeAndFlush(response);
        channel.flush();
        //ctx.close();
    }
    
	public void sendHtml(HttpServletResponse responses){
    	try {
    		Channel channel = getResponseChannel(responses.getResponseId());
    		String returnStr = responses.getResponseData();
    		File file = null;
    		if(returnStr != null && !returnStr.trim().equals("")){
				String filePath = HttpServer.cpath+viewPath+returnStr;
				for(String s : viewFileType){
					file = new File(filePath+s);
					if(file.exists()){
						break;
					}
				}
			}
    		
			if(file == null || !file.exists()){
				sendData(responses, "404",channel);
				return;
			}
			FileInputStream fis = new FileInputStream(file);
			byte[] bytes = new byte[fis.available()];
			int length = -1;
			while((length = fis.read(bytes))!=-1){
				fis.read(bytes, 0, length);
			}
			FullHttpResponse response = new DefaultFullHttpResponse(
			        HTTP_1_1, OK, Unpooled.wrappedBuffer(bytes));
			response.headers().set(CONTENT_TYPE, "text/html");
			response.headers().set(CONTENT_LENGTH,
			        response.content().readableBytes());
			response.headers().set("Access-Control-Allow-Origin","*");
			response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE);
			channel.writeAndFlush(response);
	        channel.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	public void sendHtmlFile(String htmlFilePath,Channel channel){
    	try {
    		File file = null;
    		if(htmlFilePath != null && !htmlFilePath.trim().equals("")){
				String filePath = HttpServer.cpath+viewPath+htmlFilePath;
				file = new File(filePath);
			}
    		
			if(file == null || !file.exists()){
				sendData(null, "404",channel);
				return;
			}
			FileInputStream fis = new FileInputStream(file);
			byte[] bytes = new byte[fis.available()];
			int length = -1;
			while((length = fis.read(bytes))!=-1){
				fis.read(bytes, 0, length);
			}
			FullHttpResponse response = new DefaultFullHttpResponse(
			        HTTP_1_1, OK, Unpooled.wrappedBuffer(bytes));
			response.headers().set(CONTENT_TYPE, "text/html");
			response.headers().set(CONTENT_LENGTH,
			        response.content().readableBytes());
			response.headers().set("Access-Control-Allow-Origin","*");
			response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE);
			channel.writeAndFlush(response);
	        channel.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	
	//服务请求转发
	public void sendConMethod(SystemNet.Datas.Builder datas,String path,String sessionId,Channel responseChannel){
		if(sessionId != null && !sessionId.equals("null") && !sessionId.trim().equals("")){//sessionId不能为空
			boolean hasLock = false;//是否有锁
			DistributedLock dl = null;//锁对象
			String lockid = "";//锁Id
			if(CenterServer.serverLockMap.containsKey(path)){//判断路径是否包含锁
				dl = DistributedLockMg.INSTANCE.hasAndGet(path);
				if(dl == null){
					//没获得锁不允许进入方法，此处可加入其他处理逻辑;
					datas.getServerResponseBuilder().setType("500");
					datas.getServerResponseBuilder().getServerlineDataBuilder().setSendDataStr("get lock error");
					returnToServer(datas, responseChannel);
					return;
				}
				hasLock = true;
				lockid = UUID.randomUUID().toString().replace("-", "");
			}
			MySession session = SessionMsg.INSTANCE.getSession(sessionId);//获取session
			if(session!=null&&!session.isUnUse()){
				//将各种数据写入协议，发送到各个服务器
				datas.getHttpBeanBuilder().getRequestBuilder().getSessionBuilder().setSessionId(sessionId);
				datas.getHttpBeanBuilder().getRequestBuilder().getSessionBuilder().putAllSession(session.getAttr());
				session.setIsusing(true);
				
				ConcurrentMap<String,List<CenterHandle>> serverMap = CenterServer.serverMap;
				
				if(serverMap.get(path) == null){
					datas.getServerResponseBuilder().setType("500");
					datas.getServerResponseBuilder().getServerlineDataBuilder().setSendDataStr("path not found");
					returnToServer(datas, responseChannel);
					return;
				}
				//nodes change
				//Channel channel = serverMap.get(path).get(0).getChannel();
				Channel channel = getServerChannel(serverMap.get(path));
				if(channel == null){
					datas.getServerResponseBuilder().setType("500");
					datas.getServerResponseBuilder().getServerlineDataBuilder().setSendDataStr("服务过载");//最好改为英文标识
					returnToServer(datas, responseChannel);
					return;
				}
				
				//datas.getHttpBeanBuilder().getRequestBuilder().setLocks(locksBuilder);
				datas.getServerResponseBuilder().getLocksBuilder().setId(lockid);
				System.out.println("发送锁id---"+lockid);
				channel.writeAndFlush(datas);
				
				session.setIsusing(false);
				if(hasLock){//如果已经获取了锁，则进行解锁监听
					DistributedLock.distributedLockMap.put(lockid, dl);
					dl.releaseLock();//等待解锁
				}
			}else{
				System.out.println("session is null");
				//sendData(null, "session is null",httpHandle.getCtxs().channel());
				return;
			}
		}
	}
	
	private void returnToServer(SystemNet.Datas.Builder datasBuilder,Channel channel){
		channel.writeAndFlush(datasBuilder);
	}
	
	public void returnToServer(String responseChannelId,SystemNet.Datas.Builder datasBuilder){
		if(!CenterHandle.channelMap.containsKey(responseChannelId)){
			//如果找不到回家的路,则迷路
		}
		Channel channel = CenterHandle.channelMap.get(responseChannelId);//找到回家的路
		channel.writeAndFlush(datasBuilder);
	}
	
}
