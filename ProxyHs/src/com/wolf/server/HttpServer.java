package com.wolf.server;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.wolf.center.CenterServer;
import com.wolf.locks.DistributedLockMg;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.timeout.IdleStateHandler;

public class HttpServer implements Runnable{

	public static int httpObjectAggregatornum = 63365;//包装为response，request
	public static String cpath = "";
	
	public void start(int port) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1); 
        EventLoopGroup workerGroup = new NioEventLoopGroup(1);//线程池可以慢慢调试
        try {
            ServerBootstrap b = new ServerBootstrap(); 
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) 
                    .childHandler(new ChannelInitializer<SocketChannel>() { 
                        @Override
                        public void initChannel(SocketChannel ch)
                                throws Exception {
                        	ch.pipeline().addLast(new IdleStateHandler(600, 0, 0));//心跳机制，判断是否掉线
                            // server端发送的是httpResponse，所以要使用HttpResponseEncoder进行编码
                            ch.pipeline().addLast(
                                    new HttpResponseEncoder());
                            // server端接收到的是httpRequest，所以要使用HttpRequestDecoder进行解码
                            ch.pipeline().addLast(
                                    new HttpRequestDecoder());
                            ch.pipeline().addLast(new HttpObjectAggregator(httpObjectAggregatornum));
                            ch.pipeline().addLast(
                                    new HttpServerHandler());//自定义handle
                        }
                    }).option(ChannelOption.SO_BACKLOG, 256) //服务端可连接队列
                    .childOption(ChannelOption.SO_KEEPALIVE, true);//保持连接
            System.out.println("server start");
            System.out.println("启动服务:"+port);
            ChannelFuture f = b.bind(port).sync(); 
            f.channel().closeFuture().sync();//同步关闭
        } finally {
            workerGroup.shutdownGracefully();//优雅推出netty线程池
            bossGroup.shutdownGracefully();//优雅推出netty线程池
        }
    }
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			start(5678);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//读取配置，目前还在完善
    public static void main(String[] args) throws Exception {
    	try {
    		String path = HttpServer.class.getClass().getResource("/").getPath();
    		path = java.net.URLDecoder.decode(path, "UTF-8");
    		String parentPath = path.replace("/bin/", "");
    		path = path.substring(1,path.length());
    		parentPath = parentPath.substring(1,parentPath.length());
			HttpServer server = new HttpServer();
			cpath = parentPath;
			loadWebConfig(path);
			SessionMsg.INSTANCE.scanSessions();
			DistributedLockMg.INSTANCE.scanDistributedLock();
			new Thread(server).start();
			new Thread(new CenterServer()).start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
    //读取配置，目前还在完善
    public static void loadWebConfig(String path){
    	try {
			File file = new File(path+"/WebConfig.xml");
			if(file.exists()){
				SAXReader reader = new SAXReader();
				Document document = reader.read(file);
				Element webConfigE = document.getRootElement();
				Element serverConfigE = webConfigE.element("serverConfig");
				Element sessionE = serverConfigE.element("session");
//				Element poolMinNumE = sessionE.element("poolMinNum");
//				Element poolMaxNumE = sessionE.element("poolMaxNum");
				Element sessionScanMinuteE = sessionE.element("sessionScanMinute");
				Element httpObjectAggregatorE = serverConfigE.element("httpObjectAggregator");
				Element viewE = serverConfigE.element("view");
				Element pathE = viewE.element("path");
				Element viewtypeE = viewE.element("viewtype");
				//SessionMsg.INSTANCE.setPoolMinNum(Long.parseLong(poolMinNumE.getText()));
				//SessionMsg.INSTANCE.setPoolMaxNum(Long.parseLong(poolMaxNumE.getText()));
				SessionMsg.INSTANCE.setSessionScanMinute(Long.parseLong(sessionScanMinuteE.getText()));
				SessionMsg.INSTANCE.init();
				httpObjectAggregatornum = Integer.parseInt(httpObjectAggregatorE.getText());
				ControllerProxy.viewPath = pathE.getText();
				String viewtype = viewtypeE.getText();
				ControllerProxy.viewFileType = viewtype.split(",");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
        
}
