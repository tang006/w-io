package com.wolf.server;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.wolf.center.CenterHandle;
import com.wolf.javabean.SystemNet;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AsciiString;
import io.netty.util.ReferenceCountUtil;
import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

//一次连接都会有一个handle
public class HttpServerHandler extends ChannelInboundHandlerAdapter{

	private HttpRequest request;//当前连接的request
	private AsciiString method;//读取请求的方法
	private boolean ishandle = false;//是否正在处理
	private SystemNet.Datas.Builder datas = SystemNet.Datas.newBuilder();//自定义协议
	private int readCount = 0;//断线尝试重连次数
	private ChannelHandlerContext ctxs = null;//当前连接的ChannelHandlerContext对象，用于获取channel
	private String responseId;//用于辨别发送和返回的response，因为访问各个服务器还是长连接
	public CenterHandle centerHandle;//注册中心的handle，用于转发请求
	private static AtomicInteger reqnum = new AtomicInteger();//前端请求数
	
	
    @Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
    	//System.out.println("http channel close");
		super.channelInactive(ctx);
	}
	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
			throws Exception {
		// TODO Auto-generated method stub
    	IdleStateEvent ide = (IdleStateEvent)evt;
    	//心跳检测是否断线，3次关闭(可作为配置)
    	if(ide.state() == IdleState.READER_IDLE){
    		if(readCount >= 3){
    			ctx.close();
    			return;
    		}
    		readCount ++;
    	}
	}
	@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
            throws Exception {
		System.out.println("reqnums==="+reqnum.incrementAndGet());
		System.out.println(ctx.channel().id().asLongText());
    	try{
    		if(ctxs == null){
    			ctxs = ctx;//ctx赋值给当前对象
    		}
    		if(readCount>0){
    			readCount = 0;
    		}
	    	if(msg instanceof HttpRequest){
	    		Map<String,String> map = new HashMap<String,String>();
	    		String path = "";
	    		request = (HttpRequest)msg;
	    		method = request.method().asciiName();
	    		if(method.toString().equals("GET")){//如果是GET，按GET的方式
	    			URI uri = new URI(request.uri());
	    			path = uri.getPath();
	    			
	    			if(path.equals("/favicon.ico")){//暂时退回
	    				return;
	    			}
	    			
	    			boolean downViewFile = false;
	    			String htmlFilePath = "";
	    			for(String s : ControllerProxy.INSTANCE.viewFileType){//静态资源的设置，如果结尾为静态文件，则返回文件
	    				if(path.endsWith(s)){
	    					downViewFile = true;
	    					htmlFilePath = path;
	    				}
	    			}
	    			if(downViewFile){//发送html文件
	    				ControllerProxy.INSTANCE.sendHtmlFile(htmlFilePath, ctx.channel());
	    				return;
	    			}
	    			QueryStringDecoder decoder = new QueryStringDecoder(request.uri());//获取请求数据
	    			Map<String, List<String>> dataML = decoder.parameters();//获取请求的参数
	    			for(String key : dataML.keySet()){
	    				map.put(key, dataML.get(key).get(0));
	    			}
	    			ControllerProxy.INSTANCE.sendConMethod(datas, path, map.get("sessionId")+"",this);//暂时前端写sessionId
	    			ishandle = true;
	    		}
	    	}
	    	if(msg instanceof HttpContent){
	    		Map<String,String> map = new HashMap<String,String>();
	    		String path = "";
	    		try{
		    		HttpContent content = (HttpContent)msg;
		    		if(method.toString().equals("POST")){//post请求接收，和GET差不多
			    		HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(request);
			    		decoder.offer(content);
			    		URI uri = new URI(request.uri());
			    		path = uri.getPath();
			    		List<InterfaceHttpData> list = decoder.getBodyHttpDatas();
			    		for(InterfaceHttpData it : list){
			    			Attribute data = (Attribute)it;
			    			map.put(data.getName(), data.getValue());
			    		}
			    		ControllerProxy.INSTANCE.sendConMethod(datas, path, map.get("sessionId")+"",this);
			    		ishandle = true;
		    		}
	    		}catch(Exception e){
	    			e.printStackTrace();
	    		}
	    	}
	    	if(ishandle){
	    		ishandle = false;
	    	}
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		ReferenceCountUtil.release(msg);
    	}
    }
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        ctx.close();
    }
    
	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelRegistered(ctx);
	}
	public ChannelHandlerContext getCtxs() {
		return ctxs;
	}
	public void setCtxs(ChannelHandlerContext ctxs) {
		this.ctxs = ctxs;
	}
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
 
    
    
}
