package com.wolf.controller;

import java.util.HashMap;
import java.util.Map;

import com.wolf.javabean.ReqResBean;
import com.wolf.javabean.SystemNet.HttpServletRequest;
import com.wolf.javabean.SystemNet.HttpServletResponse;
import com.wolf.javabean.SystemNet.HttpSession;
import com.wolf.server.MethodMapping;
import com.wolf.serverLine.ServerRequestBean;

public class TestController {
	
	@MethodMapping(path="/test2")
	public void test2(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1");
		rrb.setResponseKV("aa", "aa1");
		//调用/test3
		ServerRequestBean srb = new ServerRequestBean();
		try {
			Map<String,String> map = new HashMap<String,String>();
			map.put("server1message", "server1 say test2");
			long starttime = System.currentTimeMillis();
			Object o = srb.send("/test21", map, false,rrb);
			//Object o2 = srb.send("/test30", map, true,rrb.getSessionId());
			System.out.println("sendTime:"+(System.currentTimeMillis()-starttime));
			System.out.println(o+"");
			//System.out.println(o2+"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@MethodMapping(path="/test1")
	public void test1(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1");
		rrb.setResponseKV("aa", "aa1");
	}
	
	//注解，path//路径，lockNames:锁名称组，逗号隔开，但目前不推荐多锁，secondnum:锁时长，最好配置，避免死锁，geWait:获取锁等待时长
	@MethodMapping(path="/test4",lockNames="abc",secondnum=20,geWait=1)
	public void test4(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1");
		Map<String,String> map = new HashMap<String,String>();
		for(int i=0;i<100;i++){
			map.put(""+i, "abc"+i);
		}
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rrb.setResponseMap(map);
	}
	
	@MethodMapping(path="/test3")
	public void test3(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1");
		System.out.println(rrb.getRequestMap().toString());
		rrb.setResponseKV("server1message", "server1 say test3");
	}
	
	//本地测试
	@MethodMapping(path="/test50",trans=true)
	public void test50(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1 事务测试");
		ServerRequestBean srb = new ServerRequestBean();
		try {
			srb.send("/test52", new HashMap<String,String>(), true,rrb);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rrb.thransThread(1);//确认提交
	}
	
	@MethodMapping(path="/test52",trans=true)
	public void test52(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1");
		rrb.setResponseKV("aa", "aa52");
		rrb.thransThread(1);//确认提交
	}
	
	//分布式测试
	@MethodMapping(path="/test53",trans=true)
	public void test53(ReqResBean rrb,Map<String,String> sessionMap){
		System.out.println("server1 事务测试");
		ServerRequestBean srb = new ServerRequestBean();
		try {
			srb.send("/test54", new HashMap<String,String>(), true,rrb);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rrb.thransThread(1);//确认提交
	}
	
}
