package com.wolf.javabean;

import io.netty.channel.Channel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.wolf.javabean.SystemNet.HttpBean;
import com.wolf.javabean.SystemNet.HttpServletRequest;
import com.wolf.javabean.SystemNet.HttpServletResponse;
import com.wolf.javabean.SystemNet.ServerRequest;
import com.wolf.javabean.SystemNet.ServerResponse;
import com.wolf.javabean.TranNet.Trans;
import com.wolf.javabean.TranNet.TransDatas;
import com.wolf.server.LineTransServer;

public class ReqResBean {

	private String type;
	private HttpServletResponse.Builder cresponse = null;
	private HttpServletRequest crequest = null;
	private ServerRequest cserverRequest = null;
	private ServerResponse.Builder cserverResponse = null;
	private String sessionId = null;
	private Connection con = null;//开启事务为前提
	private Channel channel = null;
	private String mainId = null;
	private String serverId = null;
	private Boolean isMain = null;
	private TransDatas.Builder trans = null;
	private BlockingQueue<TransDatas> tranQueue = null;//事务线程控制
	private boolean isTran = false;//是否事务控制
	
	/**
	 * 分开统一
	 * @param type
	 * @param response
	 * @param request
	 */
	public ReqResBean(String type,Object response,Object request,String sessionId){
		this.type = type;
		this.sessionId = sessionId;
		if(type.equals("http")){
			cresponse = (HttpServletResponse.Builder)response;
			crequest = (HttpServletRequest)request;
		}else if(type.equals("server")){
			cserverResponse = (ServerResponse.Builder)response;
			cserverRequest = (ServerRequest)request;
		}
	}
	
	public Map<String,String> getRequestMap(){
		if(type.equals("http")){
			return crequest.getParameterMap();
		}else if(type.equals("server")){
			return cserverRequest.getServerlineData().getAttrMap();
		}else{
			return null;
		}
	}
	
	public void setResponseMap(Map<String,String> map){
		if(type.equals("http")){
			cresponse.putAllAttr(map);
		}else if(type.equals("server")){
			cserverResponse.getServerlineDataBuilder().putAllAttr(map);
		}
	}
	
	public void setResponseKV(String key,String value){
		if(type.equals("http")){
			cresponse.putAttr(key, value);
		}else if(type.equals("server")){
			cserverResponse.getServerlineDataBuilder().putAttr(key, value);
		}
	}
	
	public void setResponseData(String data){
		if(type.equals("http")){
			cresponse.setResponseData(data);
		}else if(type.equals("server")){
			cserverResponse.getServerlineDataBuilder().setSendDataStr(data);
		}
	}

	public String getSessionId() {
		return sessionId;
	}
	
	public void trans(Connection con,Channel channel,String mainId,String serverId,Boolean isMain){
		if(channel == null){
			try {
				throw new Exception("trans not line");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.con = con;
		this.channel = channel;
		this.mainId = mainId;
		this.serverId = serverId;
		this.isMain = isMain;
		this.trans = TransDatas.newBuilder();
		this.isTran = true;
		Trans.Builder ts = trans.getTransBuilder();
		ts.setMainId(mainId);
		ts.setServerId(serverId);
		ts.setIsmain(isMain);
		LineTransServer.transTaskMap.put(serverId, this);
		tranQueue = new ArrayBlockingQueue<>(1);
	}
	
	public boolean thransThreadInit(){
		try {
			Trans.Builder ts = trans.getTransBuilder();
			ts.setStatus(1);
			ts.setType("canCommit");//第一次确认
			channel.writeAndFlush(trans);
			TransDatas tds = tranQueue.take();
			
			System.out.println("第一次已确认");
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}
	}
	
	//返回是否有补偿，status，1是提交，0是回滚
	public void thransThread(int status){//事务过程
		//事务确认
		try {
			Trans.Builder ts = trans.getTransBuilder();
			ts.setType("commiting");//第二次确认
			System.out.println("第二次确认");
			channel.writeAndFlush(trans);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public boolean transEnd(){
		try {
			Trans.Builder ts = trans.getTransBuilder();
			TransDatas tds = tranQueue.take();
			
			System.out.println("第二次已确认");
			long rstatus = tds.getTrans().getStatus();
			if(rstatus == 0){
				System.out.println("业务事务回滚");
			}else{
				System.out.println("业务事务提交");
			}
			if(con!=null && !con.isClosed()){
				if(rstatus == 0){
					con.rollback();
				}else{
					con.commit();
				}
			}//连接超时或者被关闭后管理
			ts.setType("commitend");
			ts.setStatus(rstatus);//返回当前属性
			channel.writeAndFlush(trans);//第三次，已提交后确认
			tds = tranQueue.take();
			if(tds.getTrans().getCompensate()){//如果有补偿
				return false;
			}
			System.out.println("业务事务走完");
			if(isMain){
				System.out.println("所有事务走完");
				LineTransServer.transTaskMap.remove(mainId);
			}
			return true;
		} catch (Exception e) {//这里可能要分开不同异常处理不同问题，提交或者回滚异常和最后一次消息发送异常应该是不一样的
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}
	}
	
	public void returnTrans(TransDatas tds){
		try {
			tranQueue.put(tds);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Connection getCon() {
		return con;
	}

	public String getMainId() {
		return mainId;
	}

	public Boolean getIsMain() {
		return isMain;
	}

	public boolean isTran() {
		return isTran;
	}

	public void setTran(boolean isTran) {
		this.isTran = isTran;
	}
	
}
