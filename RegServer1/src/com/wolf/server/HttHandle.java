package com.wolf.server;

import com.wolf.javabean.ReqResBean;
import com.wolf.javabean.SystemNet;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class HttHandle extends ChannelInboundHandlerAdapter {

	//private SystemNet.Datas.Builder datasBuilder = SystemNet.Datas.newBuilder();
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		SystemNet.Datas datas = (SystemNet.Datas)msg;
		if(datas.getHandletype().equals("httpSend")){
			OtherServer.pool.execute(new Thread(new MyThread(datas,ctx)));
		}else{
			ctx.fireChannelRead(msg);
		}
	}

	class MyThread implements Runnable{

		private SystemNet.Datas datas;
		private ChannelHandlerContext ctx;
		public MyThread(SystemNet.Datas datas,ChannelHandlerContext ctx){
			this.datas = datas;
			this.ctx = ctx;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			boolean flag = true;
			while(flag){
				try {
					if(!datas.getHttpBean().getUrl().trim().equals("")){
						SystemNet.Datas.Builder datasBuilder = SystemNet.Datas.newBuilder();
						datasBuilder.getHttpBeanBuilder().setId(datas.getHttpBean().getId());//获取发送过来的数据
						ReqResBean rrb = RefController.INSTANCE.invokeMethod(datas.getHttpBean(),datasBuilder.getHttpBeanBuilder());//执行对应方法
						datasBuilder.setHandletype("HttpServletResponse");
						ctx.channel().writeAndFlush(datasBuilder);//发送返回的数据到前端,方法return基本都是html
						//ctl.get().writeAndFlush(datasBuilder);
						if(rrb != null && rrb.isTran()){
							boolean b = rrb.transEnd();//事务等待结束(补偿程序设计未完成)
							if(!b){
								System.out.println("进入补偿程序");
							}
						}
					}
					flag = false;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					flag = false;
				}finally{
					flag = false;
				}
			}
		}
		
	}
	
}
