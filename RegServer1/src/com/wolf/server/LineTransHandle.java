package com.wolf.server;

import com.wolf.javabean.TranNet.TransDatas;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class LineTransHandle extends ChannelInboundHandlerAdapter {
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelInactive(ctx);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		// TODO Auto-generated method stub
		TransDatas tds = (TransDatas)msg;
		String serverId = tds.getTrans().getServerId();
		LineTransServer.transTaskMap.get(serverId).returnTrans(tds);
		super.channelRead(ctx, msg);
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
			throws Exception {
		// TODO Auto-generated method stub
		super.userEventTriggered(ctx, evt);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("已和事务调度器建立连接");
		super.channelActive(ctx);
	}

	
	
}
