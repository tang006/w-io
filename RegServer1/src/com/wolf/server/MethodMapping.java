package com.wolf.server;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MethodMapping {

	String path() default "";//路径
	String lockNames() default "";//锁名称
	int secondnum() default 0;//锁时长
	long geWait() default 0;//获取锁等待时长
	boolean trans() default false;//是否启动事务
	int tranCommitMinute() default 10;//超时提交时长(秒)默认10秒
	int tranRollbackMinute() default 10;//超时回滚时长(秒)默认10秒
}
