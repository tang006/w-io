package com.wolf.server;

import com.wolf.javabean.SystemNet;
import com.wolf.javabean.SystemNet.SystemNetBean;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class OtherHandle extends ChannelInboundHandlerAdapter {
	
	private SystemNet.Datas.Builder datasBuilder = SystemNet.Datas.newBuilder();
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		// TODO Auto-generated method stub
		if(msg instanceof SystemNet.Datas){
			SystemNet.Datas datas = (SystemNet.Datas)msg;
			if(datas.getHandletype().equals("RegBean")){
				if(!datas.getSystemNetBean().getMessage().trim().equals("")){
					String message = datas.getSystemNetBean().getMessage();
					if(message.equals("unReg success")){
						ctx.channel().close();
					}else{
						System.out.println(message);
					}
				}
			}else{
				ctx.fireChannelRead(msg);
			}
		}else{
			super.channelRead(ctx, msg);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		super.exceptionCaught(ctx, cause);
	}
	
}
