package com.wolf.server;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.wolf.javabean.SystemNet;
import com.wolf.javabean.SystemNet.Datas;
import com.wolf.javabean.SystemNet.RegBean;
import com.wolf.javabean.SystemNet.SystemNetBean;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

public class OtherServer {

    public static ConcurrentMap<String,Map<String,Object>> controllerClass = new ConcurrentHashMap<String,Map<String,Object>>();
    public static String serverName = "server1";
    private int threadPoolNum = 4;
    public static ExecutorService pool = null;//业务池
    public static Channel regChannel;
	
	public void init(){
		pool = Executors.newFixedThreadPool(threadPoolNum);
		Bootstrap boot = new Bootstrap();
		NioEventLoopGroup group = new NioEventLoopGroup(4);
		try {
			boot.group(group)
			.channel(NioSocketChannel.class)
			.handler(new ChannelInitializer<Channel>() {

				@Override
				protected void initChannel(Channel ch) throws Exception {
					// TODO Auto-generated method stub
					ch.pipeline().addLast(new ProtobufVarint32FrameDecoder());
					ch.pipeline().addLast(new ProtobufDecoder(SystemNet.Datas.getDefaultInstance()));
					ch.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
					ch.pipeline().addLast(new ProtobufEncoder());
					ch.pipeline().addLast(new OtherHandle());//用于判断是否注册成功
					ch.pipeline().addLast(new HttHandle());//用于处理http相关请求
					ch.pipeline().addLast(new ServerChannelHandle());//用于处理服务器内部请求
				}
				
			});
			regChannel = boot.connect("localhost",9000).sync().channel();
			Datas.Builder datas = Datas.newBuilder();
			datas.setHandletype("RegBean");
			RegBean.Builder rb = datas.getRegBeanBuilder();
			rb.setId(UUID.randomUUID().toString().replace("-", ""));
			rb.setIp("localhost");
			rb.setPort("12345");
			rb.setRegtype("1");
			rb.setServerName(serverName);
			rb.setRequestnum(threadPoolNum);
			RefController.INSTANCE.controllerSetting(rb);
			datas.setRegBean(rb);
			regChannel.writeAndFlush(datas);//发送服务器数据进行注册
			regChannel.closeFuture().sync();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			group.shutdownGracefully();
		}
	}
	
	public static void main(String[] args) {
		new Thread(new LineTransServer()).start();
		new OtherServer().init();
	}
	
}
