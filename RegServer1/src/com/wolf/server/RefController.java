package com.wolf.server;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.wolf.controller.TestController;
import com.wolf.javabean.ReqResBean;
import com.wolf.javabean.SystemNet.Cpaths;
import com.wolf.javabean.SystemNet.Datas;
import com.wolf.javabean.SystemNet.HttpBean;
import com.wolf.javabean.SystemNet.HttpServletResponse;
import com.wolf.javabean.SystemNet.HttpSession;
import com.wolf.javabean.SystemNet.RegBean;
import com.wolf.javabean.SystemNet.ServerRequest;
import com.wolf.javabean.SystemNet.ServerResponse;

public enum RefController {

	INSTANCE;
	
	//控制器获取方法数据
	public void regControllers(Object o,RegBean.Builder regBean){
		Class clazz = o.getClass();
		Method[] methods = clazz.getDeclaredMethods();
		Map<String, Cpaths> pathsBuilder = new HashMap<String,Cpaths>();
		for(Method method : methods){
			if(method.isAnnotationPresent(MethodMapping.class)){
				String path = method.getAnnotation(MethodMapping.class).path();
				String lockNames = method.getAnnotation(MethodMapping.class).lockNames();
				int secondnum = method.getAnnotation(MethodMapping.class).secondnum();
				long geWait = method.getAnnotation(MethodMapping.class).geWait();
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("object", o);
				map.put("method", method);
				OtherServer.controllerClass.put(path, map);
				Cpaths.Builder cpath = Cpaths.newBuilder();
				cpath.addAllLocknames(Arrays.asList(lockNames.split(",")));
				cpath.setSecondnum(secondnum);
				cpath.setGeWait(geWait);
				pathsBuilder.put(path, cpath.build());
				//regBean.addPaths(path);
			}else{
				continue;
			}
		}
		regBean.putAllPaths(pathsBuilder);
	}
	
	//控制器注册
	public void controllerSetting(RegBean.Builder regBean){
		TestController tc = new TestController();
		regControllers(tc,regBean);
	}
	
	/**
	 * 由http调用
	 * @param httpBean
	 * @param builder
	 * @param type
	 * @throws Exception
	 */
	public ReqResBean invokeMethod(HttpBean httpBean,HttpBean.Builder builder) throws Exception{
		String url = httpBean.getUrl();
		//反射相关知识，根据url和注解配置的路径
		if(OtherServer.controllerClass.containsKey(url)){
			Map<String,Object> map = OtherServer.controllerClass.get(url);
			Object o = map.get("object");
			Method method = (Method)map.get("method");
			method.setAccessible(true);
			String returnTypeName = method.getReturnType().getName();
			HttpServletResponse.Builder response = builder.getResponseBuilder();
			
			Map<String,String> sessionMap = new HashMap<String,String>();
			sessionHandle(httpBean, builder,sessionMap);
			ReqResBean rrb = new ReqResBean("http",response,httpBean.getRequest(),httpBean.getRequest().getSession().getSessionId());//由ReqResBean统一封装request和response
			
			boolean trans = method.getAnnotation(MethodMapping.class).trans();//是否开启事务
			Connection con = null;//这里写获取jdbc连接的方式(为了得到更高性能，后续将一直采用原生jdbc)
			if(trans){//http直接请求进来的，默认为事务开始
				boolean isMain = false;
				String serverId = UUID.randomUUID().toString().replace("-", "");
				String mainId = serverId;
				isMain = true;
				rrb.trans(con, LineTransServer.transChannel, mainId, serverId, isMain);
				boolean b = rrb.thransThreadInit();
				if(!b){//事务进不了调度器则其他全部回滚
					System.out.println("事务预提交失败");
					return null;
				}
			}
			
			if(returnTypeName.equals("void")){//判断返回的类型
				method.invoke(o, rrb,sessionMap);
				response.setType("String");//返回给前端的类型
				response.setResponseId(httpBean.getResponse().getResponseId());//把发送的responseId返回，方便前端服务器辨别response
			}else{
				Object reto = method.invoke(o, rrb,sessionMap);
				if(reto instanceof String){
					response.setResponseId(httpBean.getResponse().getResponseId());
					response.setResponseData(reto+"");
					response.setType("file");
				}//其他返回类型可以自己做
			}
			response.setLocks(httpBean.getRequest().getLocks());
			builder.getRequestBuilder().getSessionBuilder().putAllSession(sessionMap);
			return rrb;
		}
		return null;
	}
	
	/**
	 * 由服务器之间调用
	 * @param httpBean
	 * @param builder
	 * @param type
	 * @throws Exception
	 */
	public ReqResBean invokeMethod(Datas datas,Datas.Builder builder) throws Exception{
		ServerRequest request = datas.getServerRequest();
		String url = request.getPath();
		//反射相关知识，根据url和注解配置的路径
		if(OtherServer.controllerClass.containsKey(url)){
			Map<String,Object> map = OtherServer.controllerClass.get(url);
			Object o = map.get("object");
			Method method = (Method)map.get("method");
			method.setAccessible(true);
			String returnTypeName = method.getReturnType().getName();
			ServerResponse.Builder response = builder.getServerResponseBuilder();
			
			Map<String,String> sessionMap = new HashMap<String,String>();
			sessionHandle(datas.getHttpBean(), builder.getHttpBeanBuilder(),sessionMap);
			ReqResBean rrb = new ReqResBean("server",response,request,datas.getHttpBean().getRequest().getSession().getSessionId());//由ReqResBean统一封装request和response
			
			boolean trans = method.getAnnotation(MethodMapping.class).trans();//是否开启事务
			Connection con = null;//这里写获取jdbc连接的方式(为了得到更高性能，后续将一直采用原生jdbc)
			if(trans){
				String mainId = datas.getTrans().getMainId();
				boolean isMain = false;
				String serverId = UUID.randomUUID().toString().replace("-", "");
				if(mainId == null || mainId.trim().equals("")){
					mainId = serverId;
					isMain = true;
				}
				rrb.trans(con, LineTransServer.transChannel, mainId, serverId, isMain);//方法内不允许设置con，但可以获取
				boolean b = rrb.thransThreadInit();
				if(!b){//事务进不了调度器则其他全部回滚
					rrb.thransThread(0);
					System.out.println("事务预提交失败，同一个mainId的事务全部回滚");
					return null;
				}
			}
			if(returnTypeName.equals("void")){//判断返回的类型
				method.invoke(o, rrb,sessionMap);
				response.setType("ServerResponse");//由注册中心处理
				response.setResponseId(datas.getServerResponse().getResponseId());//responseId是由另一端定义的业务ID
				response.setChannelId(datas.getServerResponse().getChannelId());//channelId是另一端对应的通道ID
			}
			response.setLocks(datas.getServerResponse().getLocks());
			builder.getHttpBeanBuilder().getRequestBuilder().getSessionBuilder().putAllSession(sessionMap);
			return rrb;
		}
		return null;
	}
	
	/**
	 * 将session数据加入(后期将改为根据业务服务器传session数据)
	 * @param httpBean
	 * @param builder
	 * @param map
	 */
	private void sessionHandle(HttpBean httpBean,HttpBean.Builder builder,Map<String,String> map){
		HttpSession session = httpBean.getRequest().getSession();
		Map<String,String> sessionMap = session.getSessionMap();
		map.putAll(sessionMap);
		HttpSession.Builder sessionBuilder = builder.getRequestBuilder().getSessionBuilder();
		sessionBuilder.setServerName(OtherServer.serverName);
		sessionBuilder.setSessionId(session.getSessionId());
	}
	
}
