package com.wolf.server;

import java.util.HashMap;
import java.util.Map;

import com.wolf.javabean.ReqResBean;
import com.wolf.javabean.SystemNet;
import com.wolf.javabean.SystemNet.HttpBean;
import com.wolf.javabean.SystemNet.HttpSession;
import com.wolf.javabean.SystemNet.ServerResponse;
import com.wolf.server.HttHandle.MyThread;
import com.wolf.serverLine.ServerRequestBean;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

//用来处理服务器内部调用
public class ServerChannelHandle extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		SystemNet.Datas datas = (SystemNet.Datas)msg;
		if(datas.getHandletype().equals("ServerRequest") || datas.getHandletype().equals("ServerResponse")){
			OtherServer.pool.execute(new Thread(new MyThread(datas,ctx.channel())));
		}
		else{
			ctx.fireChannelRead(msg);
		}
	}

	class MyThread implements Runnable{

		private SystemNet.Datas datas;
		private Channel channel;
		
		public MyThread(SystemNet.Datas datas,Channel channel){
			this.datas = datas;
			this.channel = channel;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			boolean flag = true;
			while(flag){
				try{
					if(!datas.getServerRequest().getPath().trim().equals("") && datas.getHandletype().equals("ServerRequest")){
						SystemNet.Datas.Builder datasBuilder = SystemNet.Datas.newBuilder();
						
						Map<String,String> map = new HashMap<String,String>();
						sessionHandle(datas.getHttpBean(), datasBuilder.getHttpBeanBuilder(), map);
						datasBuilder.setServerResponse(datas.getServerResponse());
						ReqResBean rrb = RefController.INSTANCE.invokeMethod(datas,datasBuilder);//执行对应方法
						if(datas.getServerResponse().getIsReturn()){
							datasBuilder.setHandletype("ServerResponse");
						}else{
							datasBuilder.setHandletype("ServerResponseNoReturn");
						}
						
						channel.writeAndFlush(datasBuilder);
						if(rrb != null && rrb.isTran()){
							boolean b = rrb.transEnd();//事务等待结束(补偿程序设计未完成)
							if(!b){
								System.out.println("进入补偿程序");
							}
						}
					}else if(datas.getHandletype().equals("ServerResponse")){
						String responseId = datas.getServerResponse().getResponseId();//找到家
						ServerRequestBean srb = ServerRequestBean.serRMap.get(responseId);
						ServerResponse response = datas.getServerResponse();
						if(srb!=null){
							String sendDataStr = response.getServerlineData().getSendDataStr();
							Map<String,String> map = response.getServerlineData().getAttrMap();
							if(sendDataStr!=null && !sendDataStr.trim().equals("")){
								srb.setResponse(sendDataStr);
							}else if(map!=null && map.size()>0){
								srb.setResponse(map);
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					flag = false;
				}
			}
		}
		
	}
	
	/**
	 * 将session数据加入(后期将改为根据业务服务器传session数据)
	 * @param httpBean
	 * @param builder
	 * @param map
	 */
	private void sessionHandle(HttpBean httpBean,HttpBean.Builder builder,Map<String,String> map){
		HttpSession session = httpBean.getRequest().getSession();
		Map<String,String> sessionMap = session.getSessionMap();
		map.putAll(sessionMap);
		HttpSession.Builder sessionBuilder = builder.getRequestBuilder().getSessionBuilder();
		sessionBuilder.setServerName(OtherServer.serverName);
		sessionBuilder.setSessionId(session.getSessionId());
	}
	
	
}
