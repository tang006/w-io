package com.wolf.javabean;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AllTransBean {
	
	//private ReentrantReadWriteLock wrlock = new ReentrantReadWriteLock(false);
	private List<TransBean> tbList = new ArrayList<TransBean>();//事务容器
	private Boolean allCommiting;//所有事务确定，第二次确认，只要一个事务未通过，则所有事务未通过
	private int allStatus = 0;//总状态，0是回滚，1是提交

	public void addTransBean(TransBean tb){//添加事务
		System.out.println("添加事务");
		tbList.add(tb);
		tb.getCtx().channel().writeAndFlush(tb.getDatasBuilder());
		System.out.println("添加事务完成");
	}
	
	//第二阶段，如果不是主控事务，就自己确认自己的事务
	public void commiting(String serverTranId,boolean isMain,long status){
		System.out.println("当前事务数量---"+tbList.size());
		System.out.println("确认");
		if(isMain){
			if(status == 0){
				//告诉所有服务器全部回滚
				allRollback();
				System.out.println("全部回滚");
			}else{
				allStatus = 1;
				List<TransBean> isall = new ArrayList<TransBean>();//用于保存还没进入第二步的事务，也许是断线了，可以设置等待超时程序，目前暂时不处理
				for(TransBean tb : tbList){
					if(tb.getServerTranId().equals(serverTranId)){
						tb.setCommiting(true);
						tb.setStatus(status);
					}
					if(tb.getCommiting() == null){
						isall.add(tb);//不知道什么原因未能进入第二步的事务
					}
					if(tb.getStatus() == 0){
						allStatus = 0;
					}
				}
				if(allStatus == 0){
					//告诉所有服务器全部回滚
					allRollback();
					System.out.println("全部回滚");
				}else{
					//告诉所有服务器全部提交
					allCommit();
					System.out.println("全部提交");
				}
			}
			System.out.println("确认完成");
		}else{
			for(TransBean tb : tbList){
				if(tb.getServerTranId().equals(serverTranId)){
					tb.setCommiting(true);//设置正在提交
					tb.setStatus(status);
				}
			}
		}
	}
	
	public void allCommitend(String serverTranId,boolean isMain,long status){//事务结尾，可能是回滚可能是提交
		System.out.println("事务结尾");
		if(isMain){
			boolean isAllCompensate = false;//是否全部进入补偿程序
			for(TransBean tb : tbList){
				if(tb.getServerTranId().equals(serverTranId)){
					tb.setCommitend(true);
					tb.setStatus(status);
				}
				if(tb.getStatus() != allStatus){
					//如果出现已经提交事务状态和当前事务状态不符
					if(tb.getStatus() == 1 && allStatus == 0){
						//需要回滚的时候不回滚，则需要进入补偿程序
						tb.setOtherCompensate(true);
						tb.returnServer();
						System.out.println("当前事务进入补偿");
					}else if(tb.getStatus() == 0 && allStatus == 1){
						//本身已经回滚了但其他事务却已经提交
						isAllCompensate = true;
						tb.setOtherCompensate(true);
					}
				}
			}
			if(isAllCompensate){
				for(TransBean tb : tbList){
					if(tb.getOtherCompensate() != null && tb.getOtherCompensate()){//自己已经回滚了
						continue;
					}
					tb.setCompensateTrue();
					tb.getCtx().channel().writeAndFlush(tb.getDatasBuilder());//其他系统的事务设置补偿
				}
			}else{
				for(TransBean tb : tbList){
					tb.returnServer();
				}
			}
			System.out.println("事务结尾完成");
		}else{
			for(TransBean tb : tbList){
				if(tb.getServerTranId().equals(serverTranId)){
					tb.setCommitend(true);
					tb.setStatus(status);
				}
			}
		}
	}
	
	private void allRollback(){
		allCommiting = true;
		allStatus = 0;
		for(TransBean tb : tbList){
			tb.setRollback();
			tb.returnServer();
		}
	}
	
	private void allCommit(){
		allCommiting = true;
		allStatus = 1;
		for(TransBean tb : tbList){
			tb.setCommit();
			tb.returnServer();
		}
	}
	
}
