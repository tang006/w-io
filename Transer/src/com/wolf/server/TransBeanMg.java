package com.wolf.server;

import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.wolf.javabean.AllTransBean;
import com.wolf.javabean.TranNet.Trans;
import com.wolf.javabean.TranNet.TransDatas;
import com.wolf.javabean.TransBean;

//事务控制器
public enum TransBeanMg {

	INSTANCE;
	
	public static ConcurrentMap<String, AllTransBean> allTransBean = new ConcurrentHashMap<String, AllTransBean>();
	
	public void getAllTransBean(TransDatas trans,ChannelHandlerContext ctx){
		Trans ts = trans.getTrans();
		String mainId = ts.getMainId();
		boolean isMain = ts.getIsmain();
		String type = ts.getType();
		AllTransBean atb = null;
		if(type.equals("canCommit")){
			if(isMain){
				atb = new AllTransBean();
				allTransBean.put(mainId, atb);
			}else{
				atb = allTransBean.get(mainId);
			}
			atb.addTransBean(new TransBean(ctx, ts.getServerId()));
		}else if(type.equals("commiting")){
			atb = allTransBean.get(mainId);
			atb.commiting(ts.getServerId(), isMain, ts.getStatus());
		}else if(type.equals("commitend")){
			atb = allTransBean.get(mainId);
			atb.allCommitend(ts.getServerId(), isMain, ts.getStatus());
		}
	}
	
}
